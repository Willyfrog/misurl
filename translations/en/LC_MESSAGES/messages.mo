��    -      �              �  
   �     �  >   �     =     ]     d     p     w     }     �     �  	   �     �     �  �   �  "   7     Z     `     h     n     �     �  y   �  9        K     Q  H   W  !   �  7   �  
   �       +        K  N   Q     �     �     �     �  !   �            ;     /   S  -   �  �  �     4	     A	  2   G	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  \   �	     B
     b
     h
     n
     s
     �
     �
  p   �
  4        :     >  C   B     �  2   �     �     �     �       G        `     g     �     �  "   �     �     �  (   �     �  2      Acortada a Admin Bienvenido al sistema de clasificación y acortamiento de URLs Bienvenido al sistema, %(user)s Cerrar Contraseña Correo Corta El usuario ya existe Enlace Entra Expandida Inicio Ir La direccion a la que intentas acceder ha sido eliminada del sistema. Lamentamos cualquier molestia que esto haya podido ocasionar. La url '%(url)s' ha sido reportada Legal Limpiar Lista Lista de urls de %(user)s Marcar Misurls No se pudo verificar tus credenciales para acceder a esa direccion.
 Por favor, autentiquese antes de utilizar el sistema No tiene los permisos apropiados para realizar esa accion Nueva Nuevo OOPS! este enlace no parece apuntar a ningun sitio, lo has copiado bien? OOPS! este enlace se ha eliminado PayPal. La forma rápida y segura de pagar en Internet. Registrate Reportar como inapropiada Saliendo del sistema, gracias por su visita Salir Si te ha parecido util esta pagina y quieres ayudar a pagar el ancho de banda: Ultimas Url marcada como inapropiada Url marcada como limpia Usuario Usuario %(user)s creado con exito Usuarios Volver el enlace ('%(url)s') es demasiado corto para identificarlo el enlace es demasiado corto para identificarlo hubo un error alprocesar la peticion: %(err)s Project-Id-Version:  1.0
Report-Msgid-Bugs-To: guivaya@gmail.com
POT-Creation-Date: 2012-01-17 10:31+0100
PO-Revision-Date: 2012-01-17 10:31+0100
Last-Translator: Guillermo Vaya <guivaya@gmail.com>
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 0.9.6
 Shortened to Admin Welcome to a shortening and clasifying URLs system Welcome to the system, %(user)s Close Password EMail Short User already exists Link Login Expanded Home Go That address has been deleted from our system"
"We regret any inconvenience it may cause you Url '%(url)s' has been reported Legal Unset List %(user)s's urls list Set Misurls Couldn't verify your identity to grant you access to that page  Please, log in to the system before trying again You don't have the privileges to perform that action New New OOPS! that link points to nowhere, are you sure you wrote it right? OOPS! this link's been deleted PayPal. Quickest and safest way to pay in Internet Register Report as inapropiate Logging out of the system Logout If you found this web application useful and want to help pay bandwith: Recent Url marked as inapropiate Url marked as clean User User %(user)s successfully created Users Back Link's too short to identify ('%(url)s') that link is too short An error occured while processing request: %(err)s 