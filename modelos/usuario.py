# -*- coding: UTF-8 -*-
from crypt import crypt
from sqlalchemy import Column, Integer, String
import db

class Usuario(db.Base):
    '''Clase base de usuario del sistema'''
    __tablename__ = 'usuarios'      #tabla donde guardarlo
    id = Column(Integer, primary_key=True)
    username = Column(String(80), unique=True)
    email = Column(String(120))
    password = Column(String(80))
    rol = Column(Integer)   #0: normal, 1:admin

    def __init__(self, username, email, password, rol=0):
        '''Creacion de un usuario, pero sin introducirlo aun a la bbdd'''
        self.username = username
        self.email = email
        self.rol = rol
        self.password = crypt(password, username[:2] ) #encriptar password con los 2 primeros caracteres del usuario a modo de "salt"
                                                      # seguramente haya una forma mejor

    def __repr__(self):
        '''Representacion de un usuario'''
        return '<Usuario %r, %s>' % (self.username, self.rol) #TODO: deberia escribir admin

    def auth(self, clave):
        '''Comprueba si la clave suministrada es la del usuario'''
        return self.password == crypt(clave, self.username[:2])
