# -*- coding: UTF-8 -*-
from sqlalchemy import Column, Integer, String, Table, ForeignKey
from sqlalchemy.orm import relationship
import db
import redis

rserver = redis.Redis("localhost")
pre = "tag:"
etiquetado = Table('etiquetados', db.Base.metadata,
                  Column('url_id', Integer, ForeignKey('urls.id')),
                  Column('tag_id', Integer, ForeignKey('tags.id')))

class Tag(db.Base):
    '''Clase definiendo los documentos'''
    __tablename__ = "tags"

    id = Column(Integer, primary_key=True)
    nombre = Column(String(1048))
    urls = relationship('Url', secondary=etiquetado, backref="etiquetas")

    def __init__(self, nombre):
        self.nombre = nombre

    def __repr__(self):
        return '<Tag %s>' % (self.nombre)

    def redis_tag(self):
        '''
        crea un tag en redis si no existiera
        '''
        print "Usaremos la clave %s y el valor %s" % (pre+self.nombre, self.id)
        print "resultado: %r" % rserver.get(pre+self.nombre)
        if rserver.get(pre+self.nombre) == None:
            print("Creamos en redis")
            rserver.set(pre+self.nombre, self.id) #aunque utilizado para la busqueda rapida, aprovechamos para almacenar el id
        else:
            print("no creamos en redis") #TODO: Borrar

    def etiquetar(self, url):
        #FIXME: comprobar que no exista ya
        self.urls.append(url)

def buscar_tag(bus):
    '''
    busca un tag, dado un texto parcial. Devuelve una lista
    '''
    return [(lambda x: x[len(pre):])(y) for y in rserver.keys(pre+'*'+bus+'*')]

def separar_tags(tex):
    '''
    dado un string lo separa por espacios
    '''
    return tex.rsplit()
