# -*- coding: UTF-8 -*-
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Table
from sqlalchemy.orm import relationship
import db

pertenencia = Table('pertenencias', db.Base.metadata,
                  Column('url_id', Integer, ForeignKey('urls.id')),
                  Column('user_id', Integer, ForeignKey('usuarios.id')))

class Url(db.Base):
    '''Clase definiendo los documentos'''
    __tablename__ = "urls"

    id = Column(Integer, primary_key=True)
    direccion = Column(String(10480))
    corta = Column(String(256))
    nsfw = Column(Boolean)              # apta para el trabajo?

    usuarios = relationship('Usuario', secondary=pertenencia, backref="urls")

    def __init__(self, direccion, corta=None, nsfw=False):
        self.direccion = direccion
        self.corta = corta
        self.nsfw = nsfw

    def __repr__(self):
        return '<URL %s,%s,%s>' % (self.direccion, self.corta, self.nsfw)

