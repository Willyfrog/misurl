from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

#cambiar True si se quiere que sqlalchemy de info de sus transacciones
sqldebug = False

engine = create_engine(
        'postgresql://misurls:secreto@localhost/misurls',
        convert_unicode=True,
        echo=sqldebug)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base(bind=engine)
Base.query = db_session.query_property()

# para crear la base de datos ejecutar desde el interprete de python
#
# from yourapplication.database import init_db
# init_db()
#

def init_db():
    # importar aqui todos los modulos donde se defina un objeto de la base de
    # datos, de esa manera al crear la base de datos, creara una tabla para
    # cada elemento
    import modelos.usuario
    import modelos.urls
    import modelos.tags
    Base.metadata.create_all(bind=engine)
