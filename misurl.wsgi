import sys
flaskfirst = "/sites/misurl"
if not flaskfirst in sys.path:
    sys.path.insert(0, flaskfirst)

    from app import app
    application = app
