# -*- coding: UTF-8 -*-
# modulo encargado de la gestion de urls cortas asi de como del acceso a la bbdd redis

import base64
import hashlib
import redis

maximo = 10
pre = "shurl:" #prefijo para las claves de urls

rserver = redis.Redis('localhost')

def convertir(url):
    '''
    convierte la url a base 64 con un maximo de caracteres
    maximo es el numero de caracteres que ocupara el string devuelto, relleno con 0s si hace falta
    '''
    return (base64.urlsafe_b64encode(hashlib.md5(url).digest())[:maximo]).zfill(maximo)

def clave(ind):
    '''
    dado un indice, eliminamos el tope y devolvemos una tupla con la clave y el indice
    '''
    return (ind[:maximo], int(ind[maximo:], 16))

def find(hs, url):
    '''
    comprueba la existencia de una url
    devuelve None si no existe o el indice si aparece
    no recalcula la clave porque en el 90% de los casos ya se habra hecho antes
    la clave ademas debe incluir el prefijo
    '''
    #TODO: comprobar si no seria mas rapido a traves de sql
    print ("buscando %s") % (hs)
    grupo = rserver.lrange(hs, 0, -1)
    index = next((i for i in xrange(len(grupo)) if grupo[i] == url), None)
    return index

def indexar(url, publica=True):
    '''
    Introduce la url en la base de datos nosql y devuelve un localizador
    '''
    hash_url = convertir(url)
    hs = pre + hash_url
    col = find(hs, url)
    if (col == None): #preguntar por False daria problemas, 0 es False y None tambien
        rserver.rpush(hs, url)
        col = rserver.llen(hs)-1 #colisiones
    final = "%s%x" % (hash_url, col)
    if publica:
        add_ultima(final) #metemos a la lista de ultimas
    return final

def recuperar(ind):
    '''dada una clave, encontrar la url'''
    (hs, col) = clave(ind)
    return rserver.lindex(pre+hs, col)

def add_ultima(indice):
    '''mete en la lista de ultimos anyadidos'''
    rserver.rpush('ultimos', indice)
    if rserver.llen('ultimos') > 25:
        rserver.lpop('ultimos') #sacamos el mas viejo

def ultimas():
    '''lista los ultimas urls cortas metidas'''
    return rserver.lrange('ultimos', 0, -1)

def ultimas_urls():
    '''crea una lista de pares urls corta/larga de las ultimas'''
    cortas = ultimas()
    return [(corta, recuperar(corta)) for corta in cortas]

def eliminar(ind):
    '''
    dada una clave, se elimina la url
    '''
    (hs, col) = clave(ind)
    rserver.lset(pre+hs, col, "0") #se establece a 0 para indicar que una vez hubo algo

def reportar(ind):
    '''
    un usuario notifica de url inapropiada
    '''
    rserver.sadd('banhammer',ind)

def reportadas():
    '''
    lista de urls reportadas
    '''
    return rserver.smembers('banhammer')

def desreportar(ind): #nombre raro, pero evita confunsiones con valida
    '''
    elimina de la lista de inapropiadas
    '''
    rserver.srem(ind)

