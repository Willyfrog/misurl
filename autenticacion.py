# -*- coding: UTF-8 -*-
from functools import wraps
from flask import session, url_for, flash, render_template, redirect
from modelos.usuario import Usuario
from flaskext.babel import gettext

def comprobar(user, clave):
    '''Comprobacion del login del usuario para ver si es una combinacion valida'''

    u = Usuario.query.filter(Usuario.username==user).first()
    if u and u.auth(clave):
        flash(gettext(u'Bienvenido al sistema, %(user)s', user=user))
        session['autenticado'] = True
        session['usuario'] = u.id
        session['rol'] = u.rol
    else:
        flash('Datos invalidos')
    return redirect(url_for('indice'))

def autenticar():
    '''Envia una respuesta 401 en caso de no haber pasado la comprobacion'''
    error = gettext(u'No se pudo verificar tus credenciales para acceder a esa direccion.\n Por favor, autentiquese antes de utilizar el sistema')
    return render_template('error.html', error=error, backlink=url_for('indice'))

#wrapper para autenticación
def requiere_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = session.get('autenticado')
        if auth:
            return f(*args, **kwargs)
        else:
            return autenticar()
    return decorated

def noadmin():
    error = gettext(u'No tiene los permisos apropiados para realizar esa accion')
    return render_template('error.html', error=error, backlink=url_for('indice'))

#wrapper para administracion
def solo_admin(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if session.get('rol'):
            return f(*args, **kwargs)
        else:
            return noadmin()
    return decorated
