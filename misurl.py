# -*- coding: UTF-8 -*-
from flask import (Flask,
        request,
        session,
        redirect,
        url_for,
        render_template,
        flash,
        send_from_directory,
        jsonify)
from modelos.usuario import Usuario
from modelos.urls import Url
import modelos.tags as tag
from db import db_session
from autenticacion import comprobar, requiere_auth, solo_admin
#import redis # gestion de urls acortadas
import shortie
from flaskext.babel import gettext,ngettext,Babel

#########
#
# Este es el modulo principal de la interfaz web del clasificador
# se ha querido seguir el estilo marcado por el PEP8 para facilitar su lectura
# http://www.python.org/dev/peps/pep-0008/
#
# para lanzar mediante werkzeug simplemente llamar a python clasificador.py
# esto ademas lo hace en modo debug, cambiar el parametro apropiado para que no
# lo haga.
#
##########

app = Flask(__name__)
app.config.from_pyfile('settings.cfg')
babel = Babel(app)
#app.config.from_object(__name__)

#cierre de la sesion de la base de datos al terminar el request
@app.teardown_request
def cierre_sesion(exception=None):
    db_session.remove()
####################

#localizacion
@babel.localeselector
def get_locale():
    ''' intenta adivinar la mejor localizacion si no se ha establecido'''
    #TODO: posibilidad de cambiarlo por decision de usuario
    if session.get('lan'):
        lan = 'en'
    else:
        lan = request.accept_languages.best_match(['es', 'en'])
    print "Idioma: %s" % lan
    return lan
####################

### punto de entrada ###
@app.route('/')
def indice():
    '''punto de entrada a la aplicacion'''
    return render_template("indice.html")

#######################

### login/logout del sistema ###
# cualquier @app.route que venga seguido de un @requiere_auth necesita estar
# validado en el sistema
@app.route('/login',methods=['GET','POST'])
def login():
    '''Gestion de autenticación en el sistema'''
    r = render_template('login.html')
    #en el caso por defecto vuelves al login tanto si fallas como si aun no lo has intentado
    if (request.method=='POST') and (comprobar(request.form['username'], request.form['pass'])):
        r = redirect(url_for('indice'))
    return r

@app.route('/logout')
@requiere_auth
def logout():
    '''
    Cierre de sesion en el sistema, requiere autenticacion porque no tiene
    sentido si no estas dentro y evitas tonterias
    '''
    session['autenticado'] = False
    flash(gettext(u'Saliendo del sistema, gracias por su visita'))
    return redirect(url_for('indice'))

###############################
### cambio de lenguaje ###

@app.route('/en')
def a_ingles():
    session['lan'] = 1
    print "traducimos al ingles"
    return redirect(url_for('indice'))

@app.route('/es')
def a_castellano():
    session['lan'] = 0
    print "traducimos al castellano"
    return redirect(url_for('indice'))
##########################
### documentacion daw ####
@app.route('/doc/<arch>')
def documentacion_daw(arch):
    '''
    gestion de la documentacion
    '''
    #return url_for('doc_daw',filename=arch)
    return send_from_directory('doc_daw', filename=arch)

###############################
### gestion de usuarios ###
@app.route('/u')
@requiere_auth
def lista_usuarios():
    '''lista los usuarios disponibles'''
    users = Usuario.query.all()
    return render_template("lista_usuarios.html", users=users)

@app.route('/u/nuevo')
def nuevo_usuario():
    '''muestra el formulario de nuevo usuario'''
    return render_template("nuevo_usuario.html")

#el registro es libre, si hace falta permisos, añadir @requiere_auth y @solo_admin
@app.route('/u/crear', methods=['POST'])
def crear_usuario():
    '''acepta el input de usuario'''
    if not(Usuario.query.filter(Usuario.username==request.form['username']).first()):
        #si no hay otro llamado igual, lo creamos
        u = Usuario(request.form['username'], request.form['email'], request.form['pass'])
        db_session.add(u)
        try:
            db_session.commit()
        except Exception as e:
            return render_template("error.html", error = gettext(u"hubo un error al"
                    "procesar la peticion: %(err)s", err=str(e)),
                    backlink=url_for('nuevo_usuario'))
        flash(gettext(u"Usuario %(user)s creado con exito", user=u.username))
        r = 'lista_usuarios.html'
    else:
        flash(gettext(u"El usuario ya existe"))
        r = 'nuevo_usuario.html'
    return render_template(r)

@app.route('/u/misurls')
@requiere_auth
def lista_misurls():
    '''
    lista las urls del usuario
    '''
    user = Usuario.query.filter(Usuario.id==session['usuario']).first()
    urls = [(u.corta, u.direccion) for u in user.urls]
    return render_template('lista_docs.html', lista=urls)

@app.route('/u/reportar/<url>')
@requiere_auth
def reportar(url):
    '''
    marca una url como inapropiada
    solo accesible como usuario registrado
    '''
    shortie.reportar(url)
    flash(gettext(u"La url '%(url)s' ha sido reportada",url=url))
    return render_template('indice.html')

@app.route('/a/inapropiada/<url>')
@requiere_auth
@solo_admin
def inapropiada(url):
    shortie.eliminar(url)
    flash(gettext(u"Url marcada como inapropiada"))
    return redirect(url_for('lista_inapropiadas'))

@app.route('/a/desreportar/<url>')
@requiere_auth
@solo_admin
def limpiar(url):
    shortie.desreportar(url)
    flash(gettext(u"Url marcada como limpia"))
    return redirect(url_for('lista_inapropiadas'))

@app.route('/a')
@requiere_auth
@solo_admin
def lista_inapropiadas():
    lis = shortie.reportadas()
    lista = [(c, shortie.recuperar(c)) for c in lis]
    return render_template('reportadas.html', lista=lista)

@app.route('/u/urls/<user>')
def lista_urls_user(user):
    '''
        lista las urls publicas del usuario pedido
    '''
    us = Usuario.query.filter(Usuario.username==user).first()
    urls = [(u.corta, u.direccion) for u in us.urls]
    return render_template('lista_docs_usuario.html', lista=urls,user=user)

#############################

### gestion de urls #########
@app.route('/t/cortar')
def cortaurl():
    return render_template('long.html')

@app.route('/t/add', methods=['GET','POST'])
def addurl():
    '''
    crea una nueva url. puede crearse de manera anonima o registrado en el sistema
    '''
    if request.method == 'POST':
        url = request.form['url']
    else:
        url = request.args.get('url', None)
    shurl = shortie.indexar(url)
    if not (Url.query.filter(Url.direccion==url).first()):
        u = Url(url, shurl) #si no existe, creamos
        print ("%r" % u)
        db_session.add(u)
        db_session.commit()
    if session.get('autenticado'): #si el user esta conectado, se le asigna
        us = Usuario.query.filter(Usuario.id==session.get('usuario')).first()
        us.urls.append(u)
        db_session.add(us)
        db_session.commit()

    return render_template('short.html', url=shurl)

@app.route('/t/del', methods=['POST'])
def eliminaurl():
    pass

@app.route('/t/marco/<shurl>')
def marco(shurl):
    '''
    gestion del marco superior
    '''
    if len(shurl)<10:
        return render_template("error.html", error = gettext(u"el enlace es demasiado corto para identificarlo"), backlink = url_for('ultimas_urls'))
    else:
        url = shortie.recuperar(shurl)
        if url:
            return render_template("shorthead.html", url = url, shurl = shurl)
        else:
            return render_template("error.html", error = gettext(u"OOPS! este enlace no parece apuntar a ningun sitio, lo has copiado bien?"), backlink = url_for('ultimas_urls'))

@app.route('/t')
def ultimas_urls():
    '''
        lista las 10 ultimas urls metidas
    '''
    ults = shortie.ultimas_urls()
    return render_template('lista_docs.html', lista = ults)

@app.route('/0')
def eliminada():
    '''
    acceso a una url baneada
    '''
    return render_template('error.html', error=gettext(u"La direccion a la que intentas acceder ha sido eliminada del sistema. Lamentamos cualquier molestia que esto haya podido ocasionar."), backlink=url_for('ultimas_urls'))

@app.route('/legal')
def licencia():
    '''
    acceso a la pagina de licencia y terminos de uso
    '''
    return render_template('licencia.html')

@app.route('/<shurl>')
def traducir(shurl):
    if len(shurl)<10:
        return render_template("error.html", error = gettext(u"el enlace ('%(url)s') es demasiado corto para identificarlo", url=shurl), backlink = url_for('ultimas_urls'))
    else:
        url = shortie.recuperar(shurl)
        if url:
            return render_template("redir.html", url = url, shurl = shurl)
        if url == '0':
            return render_template("error.html", error=gettext(u"OOPS! este enlace se ha eliminado"), backlink=url_for('ultimas_urls'))
        else:
            return render_template("error.html", error=gettext(u"OOPS! este enlace no parece apuntar a ningun sitio, lo has copiado bien?"), backlink=url_for('ultimas_urls'))

#############################
### ajax - server ###########

@app.route('/_settags/<url>')
def set_tags_url(url):
    '''
        funcion para meter tags a la url
        los tags son una lista de elementos separaos por comas y enviados mediante get
        si algun tag no existe, se crea
    '''
    tags = request.args.get('tags', None)
    print "Tags: %s" % tags
    for t in tags.rsplit():
        ta = tag.Tag.query.filter(tag.Tag.nombre == t).first()
        if ta == None:
            print "Creamos tag: %s" % t
            ta = tag.Tag(t) #creamos
            ta.redis_tag()
        u = Url.query.filter(Url.corta ==url).first()
        ta.urls.append(u)
        db_session.add(ta)
        db_session.commit()
    return "ok"

@app.route('/_gettags/<url>')
def get_tags_url(url):
    '''
        recupera los tags de una url
    '''
    u = Url.query.filter(Url.corta ==url).first()
    if u != None:
        print u.etiquetas
        t = ' '.join([ e.nombre for e in u.etiquetas])
    else:
        t = 'Sin tags asignados'
    print "Tags del url: %r" % t
    return jsonify(tags=t)

@app.route('/_bustags')
def buscar_tags():
    '''
        busca un tag con los caracteres parciales escritos
    '''
    bus = request.args.get('q', None)
    lista = tag.buscar_tag(bus)
    return '\n'.join(lista)


#############################
if __name__ == '__main__':
    app.run()
